import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Index() {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        axios.get('/api/users')
            .then(res => {
                setUsers(res.data);
            }).catch(err => {
                alert("Cannot fetch users info!");
            })
    }, []);

    return (
        <div className="card">
            <div className="card-header">
                <h3 className="card-title">Users</h3>
                <div className="card-tools">
                    <Link to="/admin/users/create" className="btn btn-primary btn-sm">
                        Add New
                    </Link>
                </div>
            </div>
            <div className="card-body p-0">
                <table className="table table-bordered m-0">
                    <thead className="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map(usr => (
                            <tr>
                                <td>{usr.id}</td>
                                <td>{usr.name}</td>
                                <td>{usr.email}</td>
                                <td>{usr.role}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Index;