import { Route, Routes, useNavigate } from "react-router-dom";
import NotFound from "./NotFound";
import AdminIndex from "./Index";
import UserIndex from "./users/Index";
import UserCreate from "./users/Create";
import { useEffect, useState } from "react";
import axios from "axios";
import { UserContext } from "./UserContext";
import Main from "./layouts/Main";
import "./css/adminlte.min.css";

function Admin() {
    const [user, setUser] = useState({});
    const navigate = useNavigate();

    useEffect(() => {
        axios.get('/api/user')
            .then(res => {
                setUser(res.data)

                if (res.data.role != "Admin") {
                    navigate('/denied');
                }

            })
            .catch(err => {
                location.href = '/login';
            });
    }, []);

    return (
        <UserContext.Provider value={user}>

            <Main>
                <Routes>
                    <Route path="/" element={<AdminIndex />} />
                    <Route path="users" element={<UserIndex />} />
                    <Route path="users/create" element={<UserCreate />} />
                    <Route path="*" element={<NotFound />} />
                </Routes>
            </Main>

        </UserContext.Provider>
    )
}

export default Admin;