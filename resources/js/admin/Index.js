import { useContext } from "react";
import { UserContext } from "./UserContext";
function Index() {
    const user = useContext(UserContext);

    return (
        <div>
            Admin Index
            <pre>{JSON.stringify(user)}</pre>
        </div>
    )
}

export default Index;