import { Link } from "react-router-dom";

function Header() {
    return (
        <header>
            <nav>
                <Link to="/admin">Admin</Link>
                <Link to="/admin/users">Users</Link>
                <Link to="/admin/users/create">Create Users</Link>
            </nav>
        </header>
    )
}

export default Header;