import { Link, NavLink } from "react-router-dom";

function Main({ children }) {
    return (
        <div className="wrapper">
            <nav className="main-header navbar navbar-expand navbar-white navbar-light">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" data-widget="pushmenu" href="#" role="button">
                            <i className="fas fa-bars"></i>
                        </a>
                    </li>
                    <li className="nav-item d-none d-sm-inline-block">
                        <Link to="/" className="nav-link">
                            Back to Site
                        </Link>
                    </li>
                </ul>

                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <a href="/logout" className="nav-link">Logout</a>
                    </li>
                </ul>
            </nav>

            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                <Link to="/admin" className="brand-link">
                    <span className="brand-text font-weight-light">My Project</span>
                </Link>

                <div className="sidebar">
                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                            <li className="nav-item">
                                <NavLink activeClassName="active" to="/admin" className="nav-link">
                                    <i className="nav-icon fas fa-home"></i>
                                    <p>Dashboard</p>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName="active" to="/admin/users" className="nav-link">
                                    <i className="nav-icon fas fa-users"></i>
                                    <p>Users</p>
                                </NavLink>
                            </li>

                        </ul>
                    </nav>
                </div>
            </aside>

            <div className="content-wrapper">
                <div className="content pt-3">
                    {children}
                </div>
            </div>
        </div>
    )
}

export default Main;