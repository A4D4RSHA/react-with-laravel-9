import { Link } from "react-router-dom";

function PermissionDenied() {
    return (
        <div>
            <h1>Permission Denied</h1>
            <p>You do not have permission to view this page.</p>

            <Link to="/">Go to Homepage</Link>
        </div>
    )
}

export default PermissionDenied;