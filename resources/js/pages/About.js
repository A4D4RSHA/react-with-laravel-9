import { Link } from "react-router-dom";

function About() {
    return (
        <div>
            <header>
                <Link to="/">Home</Link>
                <Link to="/about">About</Link>
            </header>

            <h3>About Page</h3>
        </div>
    )
}

export default About;