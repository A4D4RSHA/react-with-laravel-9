import { Link } from "react-router-dom";

function Index() {
    return (
        <div>
            <header>
                <Link to="/">Home</Link>
                <Link to="/about">About</Link>
            </header>

            <h3>Index Page</h3>
        </div>
    )
}

export default Index;