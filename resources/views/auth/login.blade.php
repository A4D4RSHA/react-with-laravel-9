@extends('auth.layout')
@section('title', 'Login')
@section('content')
<div style="max-width: 500px;margin:20px auto">
<div class="card">
    <div class="card-header">
        <h3 class="card-title text-center">Login</h3>
    </div>
    <div class="card-body">

        <form action="{{ route('login') }}" method="post">
            @csrf

            <div class="mb-3">
                <label for="email" class="form-label">Email Address</label>
                <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}">
                @error('password')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-check mb-3">
                <input class="form-check-input" name="remember" type="checkbox" value="1" id="remember">
                <label class="form-check-label" for="remember">Remember Me</label>
            </div>

            <button type="submit" class="btn btn-primary">
                Log In
            </button>
        </form>
        <hr/>
        Don't have an account? <a href="{{ route('register') }}">Register</a>
    </div>
</div>
</div>
@endsection